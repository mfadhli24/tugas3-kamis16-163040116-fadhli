<?php

require APPPATH . '/libraries/REST_Controller.php';

class Aksesoris extends REST_Controller
{

    public function __construct($config = "rest")
    {
        parent::__construct($config);
        $this->load->model("Aksesoris_model", "aksesoris");
    }

    public function index_get()
    {
        // ketikan source code yang ada di modul
        $id = $this->get('id');
        $cari = $this->get('cari'); 

        if ($cari != "") {
            $aksesoris = $this->aksesoris->cari($cari)->result();
        } else if ($id == ""){
            $aksesoris = $this->aksesoris->getData(null)->result();
        } else {
            $aksesoris = $this->aksesoris->getData($id)->result();
        }

        $this->response($aksesoris);
    }

    public function cari($cari)
    {
        // ketikan source code yang ada di modul
        $this->db->select("*");
        $this->db->from("barang");
        $this->db->like("nama_brg", $cari);
        $this->db->or_like("kategori", $cari);
        return $this->db->get();
    }

    public function index_put()
    {
        // ketikan source code yang ada di modul
        $nama = $this->put('nama_brg');
        $harga = $this->put('harga_brg');
        $stok = $this->put('stok_brg');
        $kategori = $this->put('kategori');
        $gambar = $this->put('gambar_brg');
        $id = $this->put('id_brg');
        $data = array(
            'nama_brg' => $nama,
            'harga_brg' => $harga,
            'stok_brg' => $stok,
            'kategori' => $kategori,
            'gambar_brg' => $gambar,
            );
        $update = $this->aksesori->update('barang', $data, 'id_brg', $this->put('id_brg'));

        if ($update) {
            $this->response(array('status' => 'success', 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    public function index_post()
    {
        // ketikan source code yang ada di modul
        $nama = $this->post('nama_brg');
        $harga = $this->post('harga_brg');
        $stok = $this->post('stok_brg');
        $kategori = $this->post('kategori');
        $gambar = $this->post('gambar_brg');
        $id = $this->post('id_brg');

        $data = array(
            'id_brg' => $id,
            'nama_brg' => $nama,
            'harga_brg' => $harga,
            'stok_brg' => $stok,
            'kategori' => $kategori,
            'gambar_brg' => $gambar
            );
        $insert = $this->aksesoris->insert($data);

        if ($insert) {
            $this->response(array('status' => 'success', 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    public function index_delete()
    {
       // ketikan source code yang ada di modul
        $id = $this->delete('id_brg');
        $delete = $this->mahasiswa->delete('barang', 'id_brg', $id);

        if ($delete) {
            $this->response(array('status' => 'success'), 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}