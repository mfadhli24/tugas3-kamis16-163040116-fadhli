<?php

require APPPATH . '/libraries/REST_Controller.php';

class Pembelian extends REST_Controller
{

    public function __construct($config = "rest")
    {
        parent::__construct($config);
        $this->load->model("Pembelian_model", "pembelian");
    }

    public function index_get()
    {
        // ketikan source code yang ada di modul
        $id = $this->get('id_pembelian');
        $cari = $this->get('cari'); 

        if ($cari != "") {
            $pembelian = $this->pembelian->cari($cari)->result();
        } else if ($id == ""){
            $pembelian = $this->pembelian->getData(null)->result();
        } else {
            $pembelian = $this->pembelian->getData($id)->result();
        }

        $this->response($pembelian);
    }

    public function cari($cari)
    {
        // ketikan source code yang ada di modul
        $this->db->select("*");
        $this->db->from("pembelian");
        $this->db->like("email_pembeli", $cari);
        return $this->db->get();
    }

    public function index_put()
    {
        // ketikan source code yang ada di modul
        $nama = $this->put('nama_brg');
        $harga = $this->put('total_harga');
        $jumlah = $this->put('jumlah');
        $tanggal = $this->put('tanggal');
        $email = $this->put('email_pembeli');
        $id_brg = $this->put('id_brg');
        $id = $this->put('id_pembelian');
        $data = array(
            'id_brg' => $id_brg,
            'email_pembeli' => $email,
            'nama_brg' => $nama,
            'jumlah' => $jumlah,
            'total_harga' => $harga,
            'tanggal' => $tanggal,
            );
        $update = $this->pembelian->pembelian('barang', $data, 'id_pembelian', $this->put('id_pembelian'));

        if ($update) {
            $this->response(array('status' => 'success', 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    public function index_post()
    {
        // ketikan source code yang ada di modul
        $nama = $this->put('nama_brg');
        $harga = $this->put('total_harga');
        $jumlah = $this->put('jumlah');
        $tanggal = $this->put('tanggal');
        $email = $this->put('email_pembeli');
        $id_brg = $this->put('id_brg');
        $id = $this->put('id_pembelian');
        $data = array(
            'id_brg' => $id_brg,
            'email_pembeli' => $email,
            'nama_brg' => $nama,
            'jumlah' => $jumlah,
            'total_harga' => $harga,
            'tanggal' => $tanggal,
            );
        $insert = $this->pembelian->insert($data);

        if ($insert) {
            $this->response(array('status' => 'success', 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    public function index_delete()
    {
       // ketikan source code yang ada di modul
        $id = $this->delete('id_pembelian');
        $delete = $this->pembelian->delete('pembelian', 'id_pembelian', $id);

        if ($delete) {
            $this->response(array('status' => 'success'), 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}